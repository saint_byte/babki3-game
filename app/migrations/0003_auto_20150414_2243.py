# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20150414_2213'),
    ]

    operations = [
        migrations.CreateModel(
            name='Proizvoditelnos',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('result', models.IntegerField(verbose_name=b'\xd0\x9f\xd1\x80\xd0\xbe\xd0\xb8\xd0\xb7\xd0\xb2\xd0\xbe\xd0\xb4\xd0\xb8\xd1\x82\xd0\xb5\xd0\xbb\xd1\x8c\xd0\xbd\xd0\xbe\xd1\x81\xd1\x82\xd1\x8c \xd0\xb2 \xd1\x80\xd1\x83\xd0\xb1\xd0\xbb\xd1\x8f\xd1\x85')),
                ('dist', models.ForeignKey(verbose_name=b'\xd0\xa0\xd0\xb0\xd0\xb9\xd0\xbe\xd0\xbd', to='app.Distsns')),
            ],
            options={
                'verbose_name': '\u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u044c',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Stat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'verbose_name': '\u0421\u0442\u0430\u0442\u0438\u0441\u0442\u0438\u043a\u0430',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TypeOfWork',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'---', max_length=128, verbose_name=b'\xd0\x9d\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5')),
            ],
            options={
                'verbose_name': '\u0420\u0430\u0431\u043e\u0442\u0430',
                'verbose_name_plural': '\u0420\u0430\u0431\u043e\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='proizvoditelnos',
            name='typeWork',
            field=models.ForeignKey(verbose_name=b'\xd0\xa0\xd0\xb0\xd0\xb1\xd0\xbe\xd1\x82\xd0\xb0', to='app.TypeOfWork'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='proizvoditelnos',
            name='worker',
            field=models.ForeignKey(verbose_name=b'\xd0\xa0\xd0\xb0\xd0\xb1\xd0\xbe\xd1\x82\xd0\xbd\xd0\xb8\xd0\xba', to='app.Worker'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='graphik',
            name='typeWork',
            field=models.ForeignKey(default=None, verbose_name=b'\xd0\xa0\xd0\xb0\xd0\xb1\xd0\xbe\xd1\x82\xd0\xb0', to='app.TypeOfWork'),
            preserve_default=True,
        ),
    ]
