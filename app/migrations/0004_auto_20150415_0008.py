# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20150414_2243'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='proizvoditelnos',
            options={'verbose_name': '\u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u044c', 'verbose_name_plural': '\u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u044c'},
        ),
        migrations.AlterModelOptions(
            name='stat',
            options={'managed': False, 'verbose_name': '\u0421\u0442\u0430\u0442\u0438\u0441\u0442\u0438\u043a\u0430', 'verbose_name_plural': '\u0421\u0442\u0430\u0442\u0438\u0441\u0442\u0438\u043a\u0430'},
        ),
        migrations.RemoveField(
            model_name='proizvoditelnos',
            name='dist',
        ),
        migrations.AddField(
            model_name='proizvoditelnos',
            name='dist1',
            field=models.ManyToManyField(related_name='dists1', verbose_name=b'\xd0\xa0\xd0\xb0\xd0\xb9\xd0\xbe\xd0\xbd\xd1\x8b', to='app.Distsns'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='proizvoditelnos',
            name='veroyatnst',
            field=models.IntegerField(default=100, help_text=b'\xd0\x95\xd1\x81\xd0\xbb\xd0\xb8 \xd1\x82\xd0\xbe\xd1\x87\xd0\xbd\xd0\xbe \xd1\x82\xd0\xbe 100', verbose_name=b'\xd0\x92\xd0\xb5\xd1\x80\xd0\xbe\xd1\x8f\xd1\x82\xd0\xbd\xd0\xbe\xd1\x81\xd1\x82\xd1\x8c \xd0\xb2 %', validators=[django.core.validators.MaxValueValidator(100), django.core.validators.MinValueValidator(1)]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='proizvoditelnos',
            name='result',
            field=models.IntegerField(help_text=b'\xd0\x9d\xd0\xb0 1000 \xd0\xbd\xd0\xb0\xd1\x81\xd0\xb5\xd0\xbb\xd0\xb5\xd0\xbd\xd0\xb8\xd1\x8f', verbose_name=b'\xd0\x9f\xd1\x80\xd0\xbe\xd0\xb8\xd0\xb7\xd0\xb2\xd0\xbe\xd0\xb4\xd0\xb8\xd1\x82\xd0\xb5\xd0\xbb\xd1\x8c\xd0\xbd\xd0\xbe\xd1\x81\xd1\x82\xd1\x8c \xd0\xb2 \xd1\x80\xd1\x83\xd0\xb1\xd0\xbb\xd1\x8f\xd1\x85'),
            preserve_default=True,
        ),
    ]
