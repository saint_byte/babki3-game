# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Worker',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128)),
                ('status', models.IntegerField(default=1, choices=[(0, b'\xd0\xa3\xd0\xb2\xd0\xbe\xd0\xbb\xd0\xb5\xd0\xbd'), (1, b'\xd0\xa0\xd0\xb0\xd0\xb1\xd0\xbe\xd1\x82\xd0\xb0\xd0\xb5\xd1\x82')])),
            ],
            options={
                'verbose_name': '\u0420\u0430\u0431\u043e\u0442\u043d\u0438\u043a',
                'verbose_name_plural': '\u0420\u0430\u0431\u043e\u0442\u043d\u0438\u043a\u0438',
            },
            bases=(models.Model,),
        ),
    ]
