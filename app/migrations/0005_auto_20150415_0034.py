# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_auto_20150415_0008'),
    ]

    operations = [
        migrations.AddField(
            model_name='proizvoditelnos',
            name='alldist',
            field=models.BooleanField(default=False, help_text=b'\xd0\x9f\xd0\xb5\xd1\x80\xd0\xb5\xd0\xbe\xd0\xbf\xd1\x80\xd0\xb5\xd0\xb4\xd0\xb5\xd0\xbb\xd1\x8f\xd0\xb5\xd1\x82 \xd0\xbf\xd0\xbe\xd0\xbb\xd0\xb5 \xd1\x80\xd0\xb0\xd0\xb9\xd0\xbe\xd0\xbd\xd1\x8b', verbose_name=b'\xd0\x92\xd1\x81\xd0\xb5 \xd1\x80\xd0\xb0\xd0\xb9\xd0\xbe\xd0\xbd\xd1\x8b'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='proizvoditelnos',
            name='dist1',
            field=models.ManyToManyField(help_text=b'', related_name='dists1', verbose_name=b'\xd0\xa0\xd0\xb0\xd0\xb9\xd0\xbe\xd0\xbd\xd1\x8b', to='app.Distsns'),
            preserve_default=True,
        ),
    ]
