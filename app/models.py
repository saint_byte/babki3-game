#-*- coding: utf-8 -*-
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
# Create your models here.

class Worker(models.Model):
    name = models.CharField(max_length=128,verbose_name="ФИО")
    status = models.IntegerField(choices=( (0,'Уволен'),(1,'Работает'),),default=1,verbose_name="Статус")
    def __unicode__(self):
        if (self.status == 0):
            return self.name.encode('utf-8') +" [Уволен]"
        return self.name.encode('utf-8')
    class Meta:
        verbose_name = "Работник"
        verbose_name_plural = "Работники"

class Distsns(models.Model):
    name = models.CharField(max_length=128,verbose_name="Название",default="---")
    peopleinsile = models.IntegerField(verbose_name="Население",default=0)
    def __unicode__(self):
        return self.name.encode('utf-8')
    class Meta:
        verbose_name = "Район"
        verbose_name_plural = "Районы"


class TypeOfWork(models.Model):
    name = models.CharField(max_length=128,verbose_name="Название",default="---")
    def __unicode__(self):
        return self.name.encode('utf-8')
    class Meta:
        verbose_name = "Работа"
        verbose_name_plural = "Работы"

class Graphik(models.Model):
    worker = models.ForeignKey('Worker',verbose_name="Работник")
    dist  = models.ForeignKey('Distsns',verbose_name="Район")
    typeWork  = models.ForeignKey('TypeOfWork',verbose_name="Работа",default=None)
    dt    = models.DateField(verbose_name="Дата работы")
    def __unicode__(self):
        return str(self.worker).encode('utf-8') + "/"+str(self.dist).encode('utf-8')+"/"+str(self.typeWork).encode('utf-8') +"/" +str(self.dt.day)+"."+str(self.dt.month)+"."+str(self.dt.year)
    class Meta:
        verbose_name = "График"
        verbose_name_plural = "График"

class Proizvoditelnos(models.Model):
    worker = models.ForeignKey(Worker,verbose_name="Работник")
    dist1  = models.ManyToManyField(Distsns,related_name="dists1",verbose_name="Районы",help_text="")
    alldist = models.BooleanField(verbose_name="Все районы",help_text="Переопределяет поле районы",default=False)
    typeWork  = models.ForeignKey(TypeOfWork,verbose_name="Работа")
    veroyatnst  = models.IntegerField(verbose_name="Вероятность в %",help_text="Если точно то 100", default=100,
        validators=[
            MaxValueValidator(100),
            MinValueValidator(1)
        ])
    result  = models.IntegerField(verbose_name="Производительность в рублях",help_text="На 1000 населения")
    def __unicode__(self):
        if (self.alldist):
            dd = 'Все районы'
        else:
            dd = ",".join(p.name for p in self.dist1.all())
        return self.worker.name.encode('utf-8')+"/"+dd.encode('utf-8')+"/"+str(self.typeWork)
    class Meta:
        verbose_name = "Производительность"
        verbose_name_plural = "Производительность"


class Stat(models.Model):
    class Meta:
        managed = False
        verbose_name = "Статистика"
        verbose_name_plural = "Статистика"