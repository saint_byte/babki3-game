# -*- coding: utf8 -*-
__author__ = 'SB'
from django.apps import AppConfig
from django.core import checks
from django.contrib.admin.checks import check_admin_app
from django.utils.translation import ugettext_lazy as _


class AppConfig(AppConfig):
    name = 'app'
    verbose_name = _("CRM")
