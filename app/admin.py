# -*- coding: utf8 -*-
from django.contrib import admin
from .models import Worker,Distsns,Graphik, TypeOfWork,Proizvoditelnos,Stat
from django.shortcuts import render
from django.db.models import Q
import datetime
# Register your models here.
class WorkerAdmin(admin.ModelAdmin):
    pass
admin.site.register(Worker,WorkerAdmin)

class DistsnsAdmin(admin.ModelAdmin):
    pass
admin.site.register(Distsns,DistsnsAdmin)

class GraphikAdmin(admin.ModelAdmin):
    pass
admin.site.register(Graphik,GraphikAdmin)

class TypeOfWorkAdmin(admin.ModelAdmin):
    pass
admin.site.register(TypeOfWork,TypeOfWorkAdmin)

class ProizvoditelnosAdmin(admin.ModelAdmin):
    pass
admin.site.register(Proizvoditelnos,ProizvoditelnosAdmin)

class StatAdmin(admin.ModelAdmin):
    def changelist_view(self, request, extra_context=None):
        now = datetime.datetime.now()
        opts = self.model._meta
        cur_mnt = int(request.GET.get('m',now.month))
        cur_y = int(request.GET.get('y',now.year))
        app_label = opts.app_label
        title = opts.verbose_name_plural
        years = range(2012,2016)
        mnts = ((1,'Январь'),(2,'Февраль'),(3,'Март'),(4,'Апрель'),(5,'Май'),(6,'Июнь'),
                (7,'Июль'),(8,'Август'),(9,'Сентябрь'),(10,'Октябрь'),(11,'Ноябрь'),(12,'Декабрь'),)
        sum = 0
        items =  Graphik.objects.filter(dt__year=cur_y,dt__month=cur_mnt)
        for item in items:
            try:
                pr = Proizvoditelnos.objects.filter(
                                             Q(worker=item.worker)
                                             &
                                             (
                                                 Q(dist1__in=item.dist1)
                                                 |
                                                 Q(alldist=True)
                                             )
                                            ).order_by('-veroyatnst')[0]  # будет надеятся на лучше и берем по большей вероятности
                print pr
                a1 = (item.dist.peopleinsile / 1000) * pr.result
                sum += a1
            except:
                # я поздравляю где-то успешно протерян показатель производительности
                pass


        context = {
            'app_label': app_label,
            'registery': {},   #список logger-ов для отображения
            'selected_logger': {},  #название выбранного logger-а
            'filter_choices': (),
            'title': title,
            'years':years,
            'mnts':mnts,
            'cur_mnt':cur_mnt,
            'cur_y': cur_y,
            'sum':sum,
            'items':items
        }
        return render(request,"stat.html", context)

admin.site.register(Stat,StatAdmin)